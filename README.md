# PHPbots

PHP bot scripts.

## Description

* Version 1.0
* Maintenance scripts for WikiSkripta.

### archiveLQT.php

* Create Archiv pages for LQT at User_talk pages and Fórum:Nástěnka.
* Archives are placed at the same page.
* Disable LQT in _LocalSettings.php_ after migration.

## Installation

* Download and place the files to /extensions/Bots/PHPbots/ folder.
* Rename _config.sample.php_ to _config.php_ and set variables.
* _logincookie.txt_ has to be writable.

## Authors and license

* [Josef Martiňák](https://www.wikiskripta.eu/w/User:Josmart)
* MIT License, Copyright (c) 2018 First Faculty of Medicine, Charles University
