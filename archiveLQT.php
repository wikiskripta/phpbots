<?php

/**
 * Create Archiv pages for LQT at User_talk pages and Fórum:Nástěnka
 */
 
class LQT {

    public $threads = array();
    public $threadsMap = array();
    public $list;
    public $bot;

    function __construct() {

        require 'bot.class.php';
        require 'config.php';
        $this->bot = new Bot($wikiurl, $botlogin, $botpassword);
        $this->bot->login();


        // Get main Forum threads
        $this->archiveThreads('Fórum:Portál');

		//$this->archiveThreads('Diskuse:Krev');
        //$this->archiveThreads('Diskuse_s_uživatelem:Josmart/Test');

        // Get all talk pages
        $talkNamespaces = array(1,3,5,7,9,11,13,15,101,103,105,107,109,275,711,829,2301,2303);
        //$talkNamespaces = array(3);
        foreach($talkNamespaces as $ns) {
            $continue = true;
            while($continue) {
                $query = array( 'action' => 'query',
                            'list' => 'allpages',
                            'apnamespace' => $ns,
                            'apfilterredir' => 'nonredirects',
                            'format' => 'json' );
                if(isset($start)) $query = array_merge($query, array('apfrom' => $start));
                $json = $this->bot->callApi($query);

                foreach($json->query->allpages as $page) {
					$this->archiveThreads($page->title);
                }

                if(!isset($json->continue)) $continue = false;
                else {
                    $start = $json->continue->apcontinue;
                }
            }

        }
		$this->bot->logout();
    }


    /**
     * Get all threads for given article name and save to archive page
     * And create link to the archive
     * @param obj $this->bot: bot class instance
     * @param string $pagename
     */
    public function archiveThreads($pagename) {
        $continue = true;
        $content = '';
        $level = 0;
        $this->threads = array();
        $this->threadsMap = array();
        $this->list = '';
        while($continue) {
            $query = array( 'action' => 'query',
                            'list' => 'threads',
                            'thprop' => 'id|subject|page|parent|ancestor|created|modified|author|summaryid|rootid|type|signature|reactions|replies',
                            'thshowdeleted' => 0,
                            'thdir' => 'newer',
                            'thrender' => 1,
                            'thpage' => $pagename,
                            'format' => 'json' );
            if(isset($start)) $query = array_merge($query, array('thstartid' => $start));
            $json = $this->bot->callApi($query);

            if( !isset($json->error) ) {

                foreach($json->query->threads as $thread) {
                    if(isset($thread->content) && !empty($thread->content)) {
                        $arr = preg_split("/<div class=\"lqt_post[^>]*>/",$thread->content);
                        $arr = preg_split("/<!--/",$arr[1]);
                        //$item = preg_replace("/\\n/", " ", $arr[0]);
                        
                        // replace <p></p>
                        $item = preg_replace("/<p>/", "", $arr[0]);
                        $item = preg_replace("/<\/p>/", "", $item);
                                                
                        // replace img
                        if(preg_match_all("/<img.*?src=\"([^\"]*)\" .*?\/>/",$item,$matches,PREG_SET_ORDER)) {
                            foreach($matches as $m) {
                                if(preg_match("/^\/images\//",$m[1])) {
                                    if(preg_match("/^\/images\/thumb\//",$m[1])) $thumb = 'thumb|'; else $thumb = '';
                                    // wikilink
                                    if(preg_match("/\/([0-9]*)px-(.*)$/",$m[1],$mm)) {
                                        $imglink = "[[File:" . $mm[2] . "|$thumb" . $mm[1] . "px]]";
                                    }
                                    else {
                                        $arr = explode("/", $m[1]);
                                        $imglink = "[[File:" . end($arr) . "|$thumb" . $mm[1] . "px]]";
                                    }
                                }
                                else {
                                    // external link
                                    $imglink = " $m[1] ";
                                }
                                $item = str_replace($m[0],$imglink,$item);
                            }
                        }
                        //replace links

                        if(preg_match_all("/<a.*?href=\"([^\"]*)\".*?>(.*?)<\/a>/",$item,$matches,PREG_SET_ORDER)) {
                            foreach($matches as $m) {

                                if(preg_match("/^http/",$m[1])) {
                                    $link = "[" . $m[1] . " " . $m[2] . "]";
                                }
                                elseif(preg_match("/^\/w\/(.*)$/",$m[1],$mm)) {
                                    if(preg_match("/\[\[/",$m[2])) {
                                        $link = $m[2];
                                    }
                                    elseif(empty($m[2])) {
                                        $link = '';
                                    }
                                    else $link = "[[" . $mm[1] . "|" . $m[2] . "]]";
                                }
                                elseif(preg_match("/^\/index\.php\/(.*)$/",$m[1],$mm)) {
                                    $link = "[[" . $mm[1] . "|" . $m[2] . "]]";
                                }
                                else $link = "[https://www.wikiskripta.eu" . $m[1] . " " . $m[2] . "]";
                                $item = str_replace($m[0],$link,$item);
                            }
                        }
                        // replace <a></a>
                        $item = preg_replace("/<a .*?>/", "", $item);
                        $item = preg_replace("/<\/a>/", "", $item);
                        
                        $item = trim($item);
                        $item .= "<br>" . $thread->signature . " " . date('j.n.Y, H:i', strtotime($thread->created));

						$thread->subject = preg_replace("/\{/","",$thread->subject);
						$thread->subject = preg_replace("/\}/","",$thread->subject);
						$thread->subject = preg_replace("/\[/","",$thread->subject);
						$thread->subject = preg_replace("/\]/","",$thread->subject);
                        if($thread->ancestor == 0) {
							// new thread
							$style = "<div class='lqtItemStyle'>";
							$this->list .= "* [[$pagename#" . $thread->subject . "|" . $thread->subject . "]]\n";
                        }
                        array_push( $this->threads, array('id'=>$thread->id,'content'=>$item,'ancestor'=>$thread->ancestor,'parent'=>$thread->parent,'subject'=>$thread->subject,'level'=>0) );
                    }
                }
                
                if(!isset($json->continue)) $continue = false;
                else {
                    $start = $json->continue->thstartid;
                }
                /*
                "id": "76",
                    "subject": "Bu\u0148ka",
                    "pagens": 102,
                    "pagetitle": "F\u00f3rum:Port\u00e1l",
                    "parent": null,
                    "ancestor": "0",
                    "created": "20121006115328",
                    "modified": "20121020165042",
                    "author": {
                        "id": "0",
                        "name": "31.192.92.4"
                    },
                    "summaryid": "41084",
                    "rootid": "40450",
                    "type": "0",
                    "signature": "[[Speci\u00e1ln\u00ed:P\u0159\u00edsp\u011bvky/31.192.92.4|31.192.92.4]]",
                    "reactions": [],
                    "replies": {
                        "80": {
                            "id": "80"
                        }
                    },
                    "content":
                    */
            }
            else $continue = false;
        }
        
        // order items
        $this->sortThreads();

        foreach($this->threadsMap as $threadId) {
            $thread = $this->threads[$threadId];
            if($thread['ancestor'] == 0) {
                // new thread
                $style = "<div class='lqtItemStyle'>";
                $content .= "=== " . $thread['subject'] . " ===\n";
            }
            else {
                $style = "<div class='lqtItemStyle' style='margin-left:" . (30*$thread['level']) . "px;'>";
            }
            $content .= $style . $thread['content'] . "</div>\n";
        }
        
		if(!empty($content)) {
			//$content = "\n\n" . "== Archiv původní diskuse ==\n\n" . $this->list . "\n\n" . $content;
			$content = "\n\n" . "== Archiv původní diskuse ==\n\n" . $content;
			//echo "<hr>".serialize($this->threadsMap);
			$this->bot->addText($pagename, $content, 'append');
		}
        
    }


    /**
     * Sort threads
     */
    public function sortThreads() {
        $root = array_filter($this->threads, function ($var) {
            return ($var['ancestor'] == 0);
        });

        foreach($root as $key=>$value) {
            array_push($this->threadsMap,$key);
            $this->getChildren($this->threads[$key]['id'],1);
        }
    }

    public function getChildren($parentId,$level) {
        $children = array_filter($this->threads, function ($var) use ($parentId) {
            return ($var['parent'] == $parentId);
        });

        foreach($children as $key=>$value) {
            array_push($this->threadsMap,$key);
            $this->threads[$key]['level'] = $level;
            //echo "####".$children[$i]['id']."#####";
            $this->getChildren($children[$key]['id'],$level+1);
        }
    }

}

$lqt = new LQT();

?>